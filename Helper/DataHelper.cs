﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Tridion.ContentManager.ContentManagement;
using Tridion.ContentManager.CoreService.Client;

namespace PostNL.DataExtenders.Helper
{
    public static class DataHelper
    {
        private static string binding = "netTcp_2013";
        private static SessionAwareCoreServiceClient client = null;
        private static XmlDocument _xmlDoc;
        /// <summary>
        /// Read ComponentData from the Core Service, via the CoreServiceHandler
        /// </summary>
        /// <param name="uri">URI of Component</param>
        /// <returns>ComponentData</returns>
        public static string GetLanguage(string uri)
        {
            //Because this project is a Class Library we can't use the ConfigurationManager to retrieve variables from config file.
            //This retrieves variables from settings file
            string pubNL = PostNL.DataExtenders.dataExtender.Default.pubNL;
            string pubEN = PostNL.DataExtenders.dataExtender.Default.pubEN;
            string result = string.Empty;
            ComponentData compData = null;
            //this is secret man
            string userName = "Administrator";
            string password = "Password";
            var credentials = CredentialCache.DefaultNetworkCredentials;
            if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
            {
                credentials = new NetworkCredential(userName, password);
            }
            
            using (var client = new SessionAwareCoreServiceClient(binding))
            {
                client.ChannelFactory.Credentials.Windows.ClientCredential = credentials;
                compData = client.Read(uri, new ReadOptions()) as ComponentData;
                string owningPubID = compData.BluePrintInfo.OwningRepository.IdRef;
                Trace.Write("[GetLanguage()] OwningRepo ID: " + owningPubID + " - " + pubEN +  " - " + pubNL);

                if (owningPubID == pubNL) result = "NL";
                else if (owningPubID == pubEN) result = "EN";
                else { result = "-"; }
            }
            return result;
        }

        /// <summary>
        /// Get Xml Doc
        /// </summary>
        /// <returns>XmlDocument</returns>
        private static XmlDocument GetXmlDoc()
        {
            if (_xmlDoc == null)
            {
                XmlDocument xmlDoc = new XmlDocument();
                _xmlDoc = xmlDoc;
            }
            return _xmlDoc;
        }
    }
}
