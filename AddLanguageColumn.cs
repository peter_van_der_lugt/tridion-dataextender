﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Tridion.Web.UI.Core.Extensibility;
using Tridion.ContentManager;
using Tridion.ContentManager.CoreService.Client;
using System.IO;
using System.Xml.Linq;
using PostNL.DataExtenders.Helper;

namespace PostNL.DataExtenders
{
    public class AddLanguageColumn : DataExtender
    {
        public override string Name
        {
            get {
                Type type = this.GetType();
                return String.Concat(type.Namespace, ".", type.Name);
            }
        }

        public override XmlTextReader ProcessRequest(XmlTextReader reader, PipelineContext context)
        {
            return reader;
        }

        public override XmlTextReader ProcessResponse(XmlTextReader reader, PipelineContext context)
        {

            XmlTextReader xReader = reader;

            TextWriter sWriter = new StringWriter();
            XmlTextWriter xWriter = new XmlTextWriter(sWriter);

            string command = context.Parameters["command"] as String;
            //CP's get retrieved by this command
            if(command == "GetItem")
            {
                xReader.MoveToContent();

                while (!xReader.EOF)
                {
                    switch (xReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            xWriter.WriteStartElement(xReader.Prefix, xReader.LocalName, xReader.NamespaceURI);
                            xWriter.WriteAttributes(xReader, false);

                            //Have to improve this
                            string id = xReader.GetAttribute("href", "http://www.w3.org/1999/xlink");  // URI
                            try
                            {
                                
                                TcmUri uri = new TcmUri(id);

                                if (uri.ItemType == Tridion.ContentManager.ItemType.Component)
                                {
                                    string value = DataHelper.GetLanguage(uri);
                                    //Adding the language attribute to the response XML
                                    xWriter.WriteAttributeString("language", value);
                                    xReader.MoveToElement();
                                }

                            }
                            catch (Exception e)
                            {
                                Trace.TraceError("[ProcessResponse()] Error for tcm: " + id + " Exception: " +  e);
                            }

                            if (xReader.IsEmptyElement)
                            {
                                xWriter.WriteEndElement();
                            }
                            break;

                        case XmlNodeType.EndElement:
                            xWriter.WriteEndElement();
                            break;
                        case XmlNodeType.CDATA:
                            // Copy CDATA node  <![CDATA[]]>
                            xWriter.WriteCData(xReader.Value);
                            break;
                        case XmlNodeType.Comment:
                            // Copy comment node <!-- -->
                            xWriter.WriteComment(xReader.Value);
                            break;
                        case XmlNodeType.DocumentType:
                            // Copy XML documenttype
                            xWriter.WriteDocType(xReader.Name, null, null, null);
                            break;
                        case XmlNodeType.EntityReference:
                            xWriter.WriteEntityRef(xReader.Name);
                            break;
                        case XmlNodeType.ProcessingInstruction:
                            xWriter.WriteProcessingInstruction(xReader.Name, xReader.Value);
                            break;
                        case XmlNodeType.SignificantWhitespace:
                            xWriter.WriteWhitespace(xReader.Value);
                            break;
                        case XmlNodeType.Text:
                            xWriter.WriteString(xReader.Value);
                            break;
                        case XmlNodeType.Whitespace:
                            xWriter.WriteWhitespace(xReader.Value);
                            break;
                    }
                    xReader.Read();
                };
                xWriter.Flush();
                
                xReader = new XmlTextReader(new StringReader(sWriter.ToString()));
                xReader.MoveToContent();
            }

            return xReader;
        }
    }
}
